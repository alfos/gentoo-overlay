alfos Gentoo overlay README
===========================

This is a repository (overlay) for Gentoo Linux, providing build
specifications (ebuilds).

To add this overlay run the following command:

layman -o https://gitlab.com/alfos/gentoo-overlay/raw/master/repositories.xml -L -a alfos
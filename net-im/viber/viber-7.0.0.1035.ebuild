# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Free calls, text and picture sharing with anyone, anywhere!"
HOMEPAGE="https://www.viber.com"
SRC_URI="https://download.cdn.viber.com/cdn/desktop/Linux/${PN}.deb -> ${P}.deb"

IUSE="+apulse"
SLOT="0"
KEYWORDS="amd64"

RDEPEND="
    apulse? ( >=media-sound/apulse-0.1.12 )
    !apulse? ( media-sound/pulseaudio )
"

S="${WORKDIR}"

src_unpack() {
    unpack ${A}
    cd ${S}
    tar -xJf data.tar.xz
}

src_install() {
    doins -r usr opt
    fowners root:audio /opt/${PN}/Viber
    fperms 755 /opt/${PN}/Viber
    fperms 755 /opt/${PN}/QtWebEngineProcess
    insinto opt/bin
    doins "${FILESDIR}/${PN}"
    fperms 755 /opt/bin/${PN}
    sed -i -e '/^Exec/s/viber/bin/; /^Exec/s/Viber/viber/' ${ED}/usr/share/applications/viber.desktop
}

# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit subversion

DESCRIPTION="Free Pascal Qt5 binding library by lazarus IDE"
ESVN_REPO_URI="https://svn.freepascal.org/svn/lazarus/trunk/lcl/interfaces/qt5/cbindings"

LICENSE="LGPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="
	dev-qt/qtx11extras
	dev-vcs/subversion
"

S="${WORKDIR}/${PN}"

src_unpack() {
    ESVN_REVISION=`echo ${PR} | awk '{ sub(/r+/, ""); print }'`
    subversion_src_unpack
}

src_configure() {
    cd ${S}
    qmake "QT += x11extras"
}

src_install() {
    make INSTALL_ROOT="${ED}" install
}

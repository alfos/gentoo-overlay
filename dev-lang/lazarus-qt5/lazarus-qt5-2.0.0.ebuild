# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit desktop

FPCVER="3.0.4"
QT5PASVER="2.6"

DESCRIPTION="Lazarus IDE is a feature rich visual programming environment emulating Delphi"
HOMEPAGE="https://www.lazarus-ide.org/"
SRC_URI="https://sourceforge.net/projects/lazarus/files/Lazarus%20Zip%20_%20GZip/Lazarus%20${PV}/lazarus-${PV}.tar.gz"

LICENSE="GPL-2 LGPL-2.1-with-linking-exception"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

COMMON_DEPEND="
	>=dev-lang/fpc-${FPCVER}[source]
	>=dev-libs/qt5pas-${QT5PASVER}
	net-misc/rsync
	>=sys-devel/binutils-2.19.1-r1:=
	!dev-lang/lazarus
"
DEPEND="${COMMON_DEPEND}"

S="${WORKDIR}/lazarus"

PATCHES=( "${FILESDIR}"/lazarus-0.9.26-fpcsrc.patch )

src_compile() {
    make clean bigide LCL_PLATFORM=qt5
}

src_install() {
    diropts -m0755
    dodir /usr/share
    rsync -a \
	--exclude="CVS"	--exclude=".cvsignore" \
	--exclude="*.ppw"	--exclude="*.ppl" \
	--exclude="*.ow"	--exclude="*.a" \
	--exclude="*.rst"	--exclude=".#*" \
	--exclude="*.~*"	--exclude="*.bak" \
	--exclude="*.orig"	--exclude="*.rej" \
	--exclude=".xvpics"	--exclude="*.compiled" \
	--exclude="killme*"	--exclude=".gdb_hist*" \
	--exclude="debian"	--exclude="COPYING*" \
	--exclude="*.app" \
	"${S}" "${ED%/}"/usr/share \
	|| die "Uanble to copy files!"

    dosym ../share/lazarus/startlazarus /usr/bin/startlazarus
    dosym ../share/lazarus/startlazarus /usr/bin/lazarus
    dosym ../share/lazarus/lazbuild /usr/bin/lazbuild
    dosym ../lazarus/images/ide_icon48x48.png /usr/share/pixmaps/lazarus.png

    make_desktop_entry startlazarus "Lazarus IDE" "lazarus"
    mv "${ED%/}"/usr/share/applications/startlazarus-${PN}.desktop \
	"${ED%/}"/usr/share/applications/lazarus.desktop
}

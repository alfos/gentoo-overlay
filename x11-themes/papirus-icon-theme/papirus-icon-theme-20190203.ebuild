# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Papirus icon theme for Linux"
HOMEPAGE="https://github.com/PapirusDevelopmentTeam/${PN}"
SRC_URI="https://github.com/PapirusDevelopmentTeam/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"
LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 x86 ~arm"

COMMON_DEPEND="
	dev-util/gtk-update-icon-cache
"
DEPEND="${COMMON_DEPEND}"

THEMES="
	Papirus
	Papirus-Dark
	Papirus-Light
	ePapirus
"

src_install() {
    insinto /usr/share/icons
    for TH in ${THEMES}
    do
	doins -r ${S}/${TH} && echo ${TH} icons theme installed
    done
}

pkg_postinst() {
    for TH in ${THEMES}
    do
	gtk-update-icon-cache -q /usr/share/icons/${TH}
    done
}

pkg_postrm() {
    gtk-update-icon-cache --ignore-theme-index
}

# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Arc KDE customization"
HOMEPAGE="https://git.io/${PN}"
SRC_URI="https://github.com/PapirusDevelopmentTeam/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"
LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 x86 ~arm"

COMMON_DEPEND="
	x11-themes/kvantum
"
DEPEND="${COMMON_DEPEND}"

src_install() {
    insinto /usr/share
    doins -r ${S}/aurorae
    doins -r ${S}/color-schemes
    doins -r ${S}/konsole
    doins -r ${S}/konversation
    doins -r ${S}/Kvantum
    doins -r ${S}/plasma
    doins -r ${S}/wallpapers
    doins -r ${S}/yakuake
}

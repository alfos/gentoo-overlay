# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="The hottest login theme around for KDE Plasma"
HOMEPAGE="https://github.com/MarianArlt/${PN}"
SRC_URI="https://github.com/MarianArlt/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"
LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 x86 ~arm"

COMMON_DEPEND="
        >=kde-plasma/plasma-desktop-5.14.5
        >=x11-misc/sddm-0.18.0
"
DEPEND="${COMMON_DEPEND}"

src_install() {
    rm -f ${S}/preview_*.png
    insinto /usr/share/sddm/themes/${PN}
    doins -r ${S}/*
}